import pytest
from pythagorean_triplets import *

@pytest.mark.parametrize("a,b,c,result",   [(3,4,5,True),
                                            (1,2,3,False),
                                            (5,4,3,True),
                                            (4,5,3,True),
                                            (5,12,13,True),
                                            (13,12,5,True),
                                            (24,143,145,True),
                                            (100,101,102,False),
                                            (10e6,10e6,10e7,'Error: provided numbers must be positive integers.'),
                                            (205,84,187,True),
                                            (-3,-4,-5,'Error: provided numbers must be positive integers.'),
                                            (-10,-11,-12,'Error: provided numbers must be positive integers.'),
                                            ]
                        )
def test_pythagorean_triplets(a,b,c,result):
    assert pythagorean_triplets(a,b,c) == result