'''
Function to test whether three provided numbers comprise a Pythagorean triplet
'''
def pythagorean_triplets(a,b,c):
    input_sorted = sorted([a,b,c])

    if any([(type(x)!=int) or (x<=0) for x in input_sorted]):
        return "Error: provided numbers must be positive integers."

    if input_sorted[0]**2 + input_sorted[1]**2 == input_sorted[2]**2:
        return True
    
    else:
        return False